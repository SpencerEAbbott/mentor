$(document).ready(function() {
    $(".tabs-menu a").click(function(event) {
        // only YOU can prevent default
        event.preventDefault();
        //when clicked, add class to tab
        $(this).parent().addClass("current");
        // remove class from tab that already has it
        $(this).parent().siblings().removeClass("current");
        // variable that is clicked hyper-links's href
        var tab = $(this).attr("href");
        // if the href doesn't match the click one, do this:
        $(".tab-content").not(tab).css("display", "none");
        // if  the href matches the clicked one, do this:
        $(tab).fadeIn();
    });
});
