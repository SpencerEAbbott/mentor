// http://ilikekillnerds.com/2014/07/how-to-basic-tasks-in-gulp-js/


// Include gulp and plugins
var gulp         = require('gulp'),
    browserSync  = require('browser-sync').create(),
    sass         = require('gulp-sass'),
    rename       = require("gulp-rename"),
    uglify       = require('gulp-uglify'),
    notify       = require("gulp-notify"),
    svgSprite    = require('gulp-svg-sprite'),
    plumber      = require('gulp-plumber')




// Static Server + watching scss/html files
gulp.task('serve', function() {

    browserSync.init({
        baseDir: "./"
    });

    gulp.watch( "assets/styles/stylesheet.css", ['sass']);
    gulp.watch( "*.html").on('change', browserSync.reload);
    gulp.watch( "assets/scripts/app.min.js").on('change', browserSync.reload);

});



// Compile Our Sass
gulp.task('sass', function() {
  gulp.src('assets/styles/stylesheet.scss')
    .pipe(plumber())
    .pipe(
      sass({
        outputStyle: 'expanded',
        debugInfo: true,
        lineNumbers: true,
        errLogToConsole: true
        }
      )
    )
    .pipe(gulp.dest('assets/styles'))
    .pipe(plumber.stop())
    .pipe(browserSync.stream());

});



// Uglify and Rename our Application JS
// Still Need to set this up for our vender folder as well
gulp.task('compress', function() {
  return gulp.src('assets/scripts/app.js')
    .pipe(plumber())
    .pipe(uglify())
    .pipe(rename(function (path) {
      path.basename += ".min";
      path.extname = ".js"
    }))
    .pipe(gulp.dest('assets/scripts'))
    .pipe(plumber.stop())
    .pipe(notify({ message: 'Javascript task complete' }));
});




// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('assets/styles/**/*.scss', ['sass']);
    gulp.watch('assets/scripts/app.js', ['compress']);
});



// Default Task
gulp.task('default', ['sass', 'compress', 'serve', 'watch']);

