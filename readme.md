# Welcome to my Code Test

## Dear Mentor Creative Group:

Thank you in advance for taking the time to meet with me and study my work. I hope our time goes well.

I've created this application as a humble static webstie, but I also used the best technologies at the same time to showcase my skill as a Front-End Developer.

I felt no need to build this out in any enviroment that would need any server, database after looking at the mock up. However, I was still very careful to write all of my code in such a way that I felt was the most modular while still putting DRY code as a high value point. I look forward to working through how I approached all of this with you.

I created the page with a desktop-first approach to be responsive and adaptive, depending on what was a better fit for each piece. I made design choices for smaller screens to ensure usability for any screen, even though I was only given a mockup for a desktop.

Here are some of the tools and languages I used to create this:

* HTML5
* SCSS/CSS3
* jQuery
* HTML5 Boilerplate
* Gulp
* Bourbon
* Bourbon Neat

## How to view:
If you want to look on your own machine, it's very simple since the site is static. Clone the repo to your directory of choice and open up "index.html" in your browser. If you look at the site on you own machine, Adobe's TypeKit Font, Gibson, will not load.

### Easier way to view:
I'll just show you during our interview.


Thank you again!

-Spencer

